#!/bin/sh

git submodule update --init
cd ./wl-ime-type
make
sudo cp wl-ime-type /usr/local/bin/
cd ..
cd ./swipeGuess
gcc swipeGuess.c -o swipeGuess
gcc mapScore.c -o mapScore
sudo cp swipeGuess /usr/local/bin/
sudo cp mapScore /usr/local/bin/
sudo cp completelyTypeWord.sh /usr/local/bin/
cd ..
cd SwipeBehaviors
gcc quick5.c -o quick5
sudo cp quick5 /usr/local/bin/
sudo cp *.sh /usr/local/bin/
cd ..
cd suggpicker
make
sudo cp suggpicker /usr/local/bin/
